# msvc-docker

## Containers

### wine

Basic wine-staging from official winehq

```sh
make wine
```


### wine64

64 bit wine env with cmake and ninja (and others later).

```sh
make wine64
```


### x11-novnc

Wine with xvfb and vnc in a browser.

```sh
make x11-novnc
```

To run

```sh
make run
```

To remove container

```sh
make clean
```
