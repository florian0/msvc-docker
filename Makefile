PWD=$(shell pwd)

WINE_TAG=florian0/winehq:staging
WINE64_TAG=florian0/wine64:staging
X11_TAG=florian0/winehq-x11-novnc:staging

wine: always
	docker build -t $(WINE_TAG) wine/ --build-arg wine_ver='winehq-staging' --build-arg wineticks_ver=20230212

rwine: always
	docker run --rm -it $(WINE_TAG)

wine64: wine always
	docker build -t $(WINE64_TAG) wine64/

rwine64: always
	docker run --rm -it $(WINE64_TAG)

x11-novnc: wine always
	docker build -t $(X11_TAG) x11-novnc/

run: x11-novnc
	docker run -it -p 8080:8080 \
		-v /run/media/florian0/dvd11:/mnt/dvd1 \
		-v $(PWD)/cache-winetricks:/root/.cache/winetricks \
		-v $(PWD)/verbs:/root/verbs \
		--name novnc_base \
		$(X11_TAG)

clean:
	docker rm novnc_base

always:

all: x11-novnc
